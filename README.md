# Boxbuild App #

This project is build using Android Studio 1.x. It currently has one module:

 * app - This is the module used to build the launcher application.


## Compilation ##

No special configuration or steps should be required if using Android Studio.

## GIT Procedure ##

We are following GitFlow for this project. No dev is to be done on the master branch, only the dev branch. Only when a release is ready should a branch be merged in to master.

After merge in to master, tag the branch with the version name and code like so.

v1.0-3.

Push master and new tag to origin.
package com.bloc.tablet_wrapper;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;

/**
 * Created by akroell on 22/04/2016.
 */
public class Util {

    public static Intent getLauncherIntent(Context c) {
        Intent intent = null;
        if (c == null) return null;
        final PackageManager packageManager = c.getPackageManager();
        for(final ResolveInfo resolveInfo:packageManager.queryIntentActivities(new Intent(Intent.ACTION_MAIN).addCategory(Intent.CATEGORY_HOME), PackageManager.MATCH_DEFAULT_ONLY))
        {
            if(!c.getPackageName().equals(resolveInfo.activityInfo.packageName))  //if this activity is not in our activity (in other words, it's another default home screen)
            {
                intent = packageManager.getLaunchIntentForPackage(resolveInfo.activityInfo.packageName);
                break;
            }
        }
        return intent;
    }
}

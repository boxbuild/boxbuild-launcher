 package com.bloc.tablet_wrapper;

 import android.app.Activity;
 import android.content.Context;
 import android.content.Intent;
 import android.graphics.Color;
 import android.media.AudioManager;
 import android.net.Uri;
 import android.os.Build;
 import android.os.Bundle;
 import android.os.Handler;
 import android.util.Log;
 import android.view.Menu;
 import android.view.View;
 import android.view.View.OnClickListener;
 import android.view.Window;
 import android.view.WindowManager;
 import android.widget.Button;

 import com.bloc.launcher.R;

 import java.lang.reflect.Method;

public class MainActivity extends Activity {
    private Context mContext;

    private final String TAG = MainActivity.class.getCanonicalName();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        mContext = this;
		// DESTROY ALL PENDING REBOOT INTENTS
        //Intent mStartActivity = new Intent(getBaseContext(), MainActivity.class);
        //int mPendingIntentId = 111111;
        //PendingIntent mPendingIntent = PendingIntent.getActivity(getBaseContext(), mPendingIntentId, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT );
        //AlarmManager mgr = (AlarmManager)getBaseContext().getSystemService(Context.ALARM_SERVICE);
        //mgr.cancel(mPendingIntent);
        
        // removes the title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        
        //remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
        setContentView(R.layout.activity_launcher);
        
        final View rootView = getWindow().getDecorView();
        rootView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE);
        rootView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        //rootView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);
        
        // keep the screen on
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        
        //Thread.setDefaultUncaughtExceptionHandler(onRuntimeError);
        
        
        Button btn1 = (Button) findViewById(R.id.btn1);
        
        btn1.setVisibility(View.VISIBLE);
        btn1.setBackgroundColor(Color.TRANSPARENT);
        btn1.setSoundEffectsEnabled(false);        
        
        btn1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent launchIntent = getPackageManager()
                        .getLaunchIntentForPackage("com.boxbuild.internalrcs");

                launchIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

                startActivity(launchIntent);

				resetClickCounts();
			}
		});
        

        Button btn2 = (Button) findViewById(R.id.btn2);
        
        btn2.setVisibility(View.VISIBLE);
        btn2.setBackgroundColor(Color.TRANSPARENT);
        btn2.setSoundEffectsEnabled(false);

        /** Start settings **/
        btn2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
                clickCount1++;
                if (clickCount1 == 10) {
                    /*
                    // start launcher
                    getPackageManager().clearPackagePreferredActivities(getPackageName());
                    Intent startMain = new Intent(Intent.ACTION_MAIN);
                    startMain.addCategory(Intent.CATEGORY_HOME);
                    startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(startMain);
                    resetClickCounts();*/

                    startActivity(new Intent(android.provider.Settings.ACTION_SETTINGS));
                }
            }
		});
        
        
        Button btn3 = (Button) findViewById(R.id.btn3);
        
        btn3.setVisibility(View.VISIBLE);
        btn3.setBackgroundColor(Color.TRANSPARENT);
        btn3.setSoundEffectsEnabled(false);

        /** Start browser **/
        btn3.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {

                Intent i = new Intent();
                i.setAction("com.boxbuild.INCOMING_CALL");
                sendBroadcast(i);


                clickCount2++;
				if(clickCount2==10){
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com"));
                    startActivity(browserIntent);
					resetClickCounts();
				}				

			}
		});

        Button btn4 = (Button) findViewById(R.id.btn4);
        
        btn4.setVisibility(View.VISIBLE);
        btn4.setBackgroundColor(Color.TRANSPARENT);
        btn4.setSoundEffectsEnabled(false);

        /** Restart device **/
        btn4.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				clickCount3++;
				if(clickCount3==10){
					
					try {
			            Process proc = Runtime.getRuntime().exec(new String[] { "su", "-c", "reboot" });
			            proc.waitFor();
			        } catch (Exception ex) {
			            Log.i(TAG, "Could not reboot", ex);
			        }
					
					
				}				

			}
		});

        Button btn5 = (Button) findViewById(R.id.btn5);

        btn5.setVisibility(View.VISIBLE);
        btn5.setBackgroundColor(Color.TRANSPARENT);
        btn5.setSoundEffectsEnabled(false);

        /** Start AdminPanel **/
        btn5.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i=new Intent();
                i.setAction("com.boxbuild.internalrcs.action.admin");

                clickCount4++;
                if(clickCount4==10){
                    startActivity(i);
                    clickCount4 = 0;
                }

            }
        });
        
        
        /*
        // get list of installed apps
        final PackageManager pm = getPackageManager();
        List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);
        for( ApplicationInfo packageInfo : packages ){
        	Log.e("PACKAGE::", "Installed package :" + packageInfo.packageName );
        	Log.e("PACKAGE::", "Launch Activity :" + pm.getLaunchIntentForPackage(packageInfo.packageName) );
        }
        */
        
    }
    
    //Intent launchIntent;
    
    int clickCount1,clickCount2,clickCount3,clickCount4=0;
	public void resetClickCounts(){
		clickCount1=clickCount2=clickCount3=0;
	}
    
    
    
    @Override
    public void onBackPressed() {
    	return;
    }
    
    public void onWindowFocusChanged(boolean hasFocus)
    {
        
    	try
        {
           if(!hasFocus)
           {
                Object service  = getSystemService("statusbar");
                Class<?> statusbarManager = Class.forName("android.app.StatusBarManager");
                Method collapse = statusbarManager.getMethod("collapse");
                collapse.setAccessible(true);
                collapse.invoke(service);
                
                //
                //final View rootView = getWindow().getDecorView();
                //rootView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE);
                //rootView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

           }
        }catch(Exception ex){}

    	resetClickCounts(); // NOTE - maybe not required
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        muteSystemSounds(true);
    }

    public void muteSystemSounds(boolean mute ) {
        AudioManager am =
                (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M ) {
            am.setStreamMute(AudioManager.STREAM_NOTIFICATION,mute);
            am.setStreamMute(AudioManager.STREAM_SYSTEM,mute);
        } else {
            am.setStreamVolume(
                    AudioManager.STREAM_NOTIFICATION,
                    AudioManager.ADJUST_MUTE,
                    0);
            am.setStreamVolume(
                    AudioManager.STREAM_SYSTEM,
                    AudioManager.ADJUST_MUTE,
                    0);
        }
    }

}